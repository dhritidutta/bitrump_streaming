import { Component, OnInit } from "@angular/core";
import { BodyService } from "../body.service";
import { HttpClient } from "@angular/common/http";
import { CoreDataService } from "../core-data.service";
import * as $ from "jquery";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";
@Component({
  selector: "app-identity-verification",
  templateUrl: "./identity-verification.component.html",
  styleUrls: ["./identity-verification.component.css"]
})
export class IdentityVerificationComponent implements OnInit {
  public theradioI: boolean;
  public theradioB: boolean;
  private usedtlapi: Subscription;
  private kyc2api:Subscription;
  private userupdatedocapi:Subscription;
  constructor(
    public main: BodyService,
    private http: HttpClient,
    public data: CoreDataService,
    private modalService: NgbModal,
    private route: Router
  ) { }
  // public contries: any;
  // currentDate: any = new Date().toISOString().slice(0, 10);
  // currentYear: any = new Date().getFullYear();
  // currentMonth: any = new Date().getMonth() + 1;
  // currentDay: any = new Date().getDate();

  ngOnInit() {
    this.main.getDashBoardInfo();
    this.getIdentityDetails();

  }
  panCardPic: any;
  aadharCardFront: any;
  aadharCardBack: any;
  bankStatement: any;
  taxStatement: any;


  getIdentityDetails() {

    this.data.alert("Loading...", "dark");
    var identityObj = {};
    identityObj["userId"] = localStorage.getItem("user_id");
    var jsonString = JSON.stringify(identityObj);
    // wip(1);
    this.usedtlapi = this.http
      .post<any>(this.data.WEBSERVICE + "/user/GetUserDetails", jsonString, {
        headers: {
          "Content-Type": "application/json",
          authorization: "BEARER " + localStorage.getItem("access_token")
        }
      })
      .subscribe(
        response => {
          // wip(0);
          var result = response;
          //   console.log(response);
          if (result.error.error_data) {
            this.data.alert(result.error.error_msg, "danger");
          } else {
            //$('.pan_card').val(result.userResult.ssn);

            if (result.userListResult[0].profilePic != "") {
              this.main.profilePic =
                this.data.WEBSERVICE +
                "/user/" +
                localStorage.getItem("user_id") +
                "/file/" +
                result.userListResult[0].profilePic +
                "?access_token=" +
                localStorage.getItem("access_token");
              if (result.userListResult[0].userTierType == 2 || result.userListResult[0].userTierType == 3) {
                document.getElementById('tiertwo').style.display = 'block';
              }
              else {
                //  $("#tiertwo").style.display="none" ;  
                document.getElementById('tiertwo').style.display = 'none';
              }
            } else {
              this.main.profilePic = "./assets/img/default.png";
            }
            if(result.userListResult[0].userType==1){
              (<HTMLInputElement>document.getElementById("Itype")).checked = true;
              (<HTMLInputElement>document.getElementById("Btyp")).checked = false;
            }
            else{
              (<HTMLInputElement>document.getElementById("Btyp")).checked = true;
              (<HTMLInputElement>document.getElementById("Itype")).checked = false
            }
            (<HTMLInputElement>document.getElementById("tinno")).value=result.userListResult[0].tin;
            this.bankStatement=result.userListResult[0].bankStatement;
            this.taxStatement=result.userListResult[0].taxStatement;
            //tin
            if (result.userListResult[0].idProofDoc != "") {
              this.panCardPic =
                this.data.WEBSERVICE +
                "/user/" +
                localStorage.getItem("user_id") +
                "/file/" +
                result.userListResult[0].idProofDoc +
                "?access_token=" +
                localStorage.getItem("access_token");
            } else {
              this.panCardPic = "./assets/img/file-empty-icon.png";
            }
            if (result.userListResult[0].addressProofDoc != "") {
              this.aadharCardFront =
                this.data.WEBSERVICE +
                "/user/" +
                localStorage.getItem("user_id") +
                "/file/" +
                result.userListResult[0].addressProofDoc +
                "?access_token=" +
                localStorage.getItem("access_token");
            } else {
              this.aadharCardFront = "./assets/img/file-empty-icon.png";
            }
            if (result.userListResult[0].addressProofDoc2 != "") {
              this.aadharCardBack =
                this.data.WEBSERVICE +
                "/user/" +
                localStorage.getItem("user_id") +
                "/file/" +
                result.userListResult[0].addressProofDoc2 +
                "?access_token=" +
                localStorage.getItem("access_token");
            } else {
              this.aadharCardBack = "./assets/img/file-empty-icon.png";
            }
            if (result.userListResult[0].taxStatement != null) {
              document.getElementById('docname1').style.display = 'block';
            }
            if (result.userListResult[0].bankStatement != null) {
              document.getElementById('docname2').style.display = 'block';
            }
              //   this.taxStatement =
            // if (result.userListResult[0].taxStatement != null) {
            //   this.taxStatement =
            //     this.data.WEBSERVICE +
            //     "/user/" +
            //     localStorage.getItem("user_id") +
            //     "/file/" +
            //     result.userListResult[0].taxStatement +
            //     "?access_token=" +
            //     localStorage.getItem("access_token");
            // } else {
            //   this.aadharCardBack = "./assets/img/file-empty-icon.png";
            // }
            // if (result.userListResult[0].bankStatement != null) {
            //   this.bankStatement =
            //     this.data.WEBSERVICE +
            //     "/user/" +
            //     localStorage.getItem("user_id") +
            //     "/file/" +
            //     result.userListResult[0].taxStatement +
            //     "?access_token=" +
            //     localStorage.getItem("access_token");
            // } else {
            //   this.aadharCardBack = "./assets/img/file-empty-icon.png";
            // }
          }
        },
        function (reason) {
          // wip(0);
          if (reason.data.error == "invalid_token") {
            this.data.logout();
          } else {
            this.data.logout();
            this.data.alert("Could Not Connect To Server", "danger");
          }
        },
        () => {
          this.data.loader = false;
        }
      );
  }
  radioOn() {
    (<HTMLInputElement>document.getElementById("Btyp")).checked = false;
    (<HTMLInputElement>document.getElementById("Itype")).checked = true;
    //document.getElementById("Btyp").=true;
  }
  radioOff() {
    (<HTMLInputElement>document.getElementById("Btyp")).checked = true;
    (<HTMLInputElement>document.getElementById("Itype")).checked = false
  }
  tiretwoSubmit() {
    this.data.alert("Loading...", "dark");
    var ft = new FormData();
    var selValue = $("input[type='radio']:checked").val();
    var useId = localStorage.getItem("user_id");
    var timno = ($(".tinno").val()).toString();
    var file5 = $(".bank_statement_doc")[0].files[0];
    var file6 = $(".tax_statement_pic")[0].files[0];
    if (selValue != undefined && useId != undefined && timno != undefined && file5 != undefined && file6 != undefined) {
      ft.append("userType", selValue);
      ft.append("userId", localStorage.getItem("user_id"));
      ft.append("tin", timno);
      if ($(".bank_statement_doc")[0].files[0] != undefined) {
        ft.append("bankStatement", $(".bank_statement_doc")[0].files[0]);
      } else {
        ft.append("bankStatement", "");
      }

      if ($(".tax_statement_pic")[0].files[0] != undefined) {
        ft.append("taxStatement", $(".tax_statement_pic")[0].files[0]);
      } else {
        ft.append("taxStatement", "");
      }

   this.kyc2api=this.http
        .post<any>(this.data.WEBSERVICE + "/user/submitKyc", ft, {
          headers: {
            authorization: "BEARER " + localStorage.getItem("access_token")
          }
        })
        .subscribe(
          response => {
            // wip(0);
            this.data.loader = false;
            var result = response;
            if (result.error.error_data != "0") {
              this.data.alert(result.error.error_msg, "dark");
            } else {
              this.data.alert(
                "Identity Verification Documents for Tier 3 Submitted",
                "success"
              );
              this.getIdentityDetails();
            }

          })
    }
    else {
      this.data.loader = false;
      this.data.alert("Please submit a detail to update", "warning");
    }
  }
  upload(content) {
    //console.log("form submit");
    if (
      $(".profile_pic")[0].files[0] != undefined ||
      $(".pan_card_pic")[0].files[0] != undefined ||
      $(".aadhar_card_front_side")[0].files[0] != undefined ||
      $(".aadhar_card_back_side")[0].files[0] != undefined
    ) {
      this.modalService.open(content);
    } else {

      this.data.alert("Please submit a detail to update", "warning");
    }
  }

  // docCountry:'';
  // dateofbirth;
  // doctype;
  uploadDocs() {

    this.data.alert("Loading...", "dark");
    var fd = new FormData();
    fd.append("userId", localStorage.getItem("user_id"));
    fd.append("ssn", $(".pan_card").val());
    if ($(".profile_pic")[0].files[0] != undefined) {
      fd.append("profile_pic", $(".profile_pic")[0].files[0]);
    } else {
      fd.append("profile_pic", "");
    }
    if ($(".pan_card_pic")[0].files[0] != undefined) {
      fd.append("id_proof_doc", $(".pan_card_pic")[0].files[0]);
    } else {
      fd.append("id_proof_doc", "");
    }
    if ($(".aadhar_card_front_side")[0].files[0] != undefined) {
      fd.append("address_proof_doc", $(".aadhar_card_front_side")[0].files[0]);
    } else {
      fd.append("address_proof_doc", "");
    }
    if ($(".aadhar_card_back_side")[0].files[0] != undefined) {
      fd.append("address_proof_doc_2", $(".aadhar_card_back_side")[0].files[0]);
    } else {
      fd.append("address_proof_doc_2", "");
    }

 this.userupdatedocapi=this.http.post<any>(this.data.WEBSERVICE + "/user/UpdateUserDocs", fd, {
        headers: {
          Authorization: "BEARER " + localStorage.getItem("access_token")
        }
      })
      .subscribe(
        result => {
          this.data.loader = false;

          if (result.error.error_data != "0") {
            this.data.alert(result.error.error_msg, "dark");
          } else {
            this.data.alert(
              "Identity Verification Documents Submitted",
              "success"
            );
            this.getIdentityDetails();
          }
        },
        error => {
          this.data.alert(error.error.error_description, "danger");
        }
      );
  }

  getSize(content) {
    debugger;
    //   alert('get size')
    var sz = $('#' + content)[0].files[0];
    // console.log(sz);

    if (sz.type == "image/jpeg") {
      if (sz.size > 2000000) {
        this.data.alert('File size should be less than 2MB', 'warning');
        $('#' + content).val('');
      }
    }
    else {
      this.data.alert('File should be in JPG or JPEG. ' + sz.type.split('/')[1].toUpperCase() + ' is not allowed', 'warning');
      $('#' + content).val('');
    }
  }
  getSizeforT2(content) {
    
    //   alert('get size')
    var sz = $('#' + content)[0].files[0];
    // console.log(sz);

    if (sz.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" || sz.type == "application/pdf") {
      if (sz.size > 6000000) {
        this.data.alert('File size should be less than 6MB', 'warning');
        $('#' + content).val('');
      }
    }
    else {
      this.data.alert('File should be in DOC or PDF. ' + sz.type.split('/')[1].toUpperCase() + ' is not allowed', 'warning');
      $('#' + content).val('');
    }
  }
   ngOnDestroy() {
    if (this.usedtlapi != undefined) {
      this.usedtlapi.unsubscribe();
    }
    if (this.kyc2api != undefined) {
      this.kyc2api.unsubscribe();
    }
    if (this.userupdatedocapi != undefined) {
      this.userupdatedocapi.unsubscribe();
    }
  }
}