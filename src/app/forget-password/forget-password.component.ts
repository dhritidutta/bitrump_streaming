import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { HttpClient } from '@angular/common/http';
import { CoreDataService } from '../core-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {
  email: any;
  password: any;
  secure_token: any;
  repassword: any;
  forgetpwdObj:any={};

  constructor(private http:HttpClient, public data:CoreDataService, private route:Router) { }

  ngOnInit() {
    
  }

  confirmPassword(e) {
    if (
      (this.forgetpwdObj.password != '' && this.forgetpwdObj.password != undefined) &&
      (this.forgetpwdObj.repassword1 != '' && this.forgetpwdObj.repassword1 != undefined)
    ) {
      if (this.forgetpwdObj.password == this.forgetpwdObj.repassword1) {
        $('.confirm_password_text').html('Password Matched');
        $('.confirm_password_text').css('color', 'lightgreen');
        $('#submit_btn').removeAttr('disabled');
      } else {
        $('.confirm_password_text').html('Password  Mismatched');
        $('.confirm_password_text').css('color', 'red');
        $('#submit_btn').attr('disabled', 'disabled');
      }
    } else {
  
    }
  
  }
  checkPassword() {
    var password = this.forgetpwdObj.password;
    if (password != '' && password != undefined && password.length >= 8) {
      var passwordStatus = this.checkAlphaNumeric(password);
      if (passwordStatus == false) {
        this.data.alert('Password should have atleast one upper case letter, one lowercase letter , one special case and one number', 'warning');
      } else {
      }
    }
    else {
      this.data.alert('Password should be minimum 8 Charecter', 'warning');
    }
  }
  checkAlphaNumeric(string) {
    if (string.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/)) {
      return true;
    } else {
      return false;
    }
  }
  forgotPassword(isValid){

     if(isValid){
      // $('.submit_btn').removeAttr('disabled');
      //  var forgotPasswordObj={};
      //  forgotPasswordObj['email']=this.email;
      //  forgotPasswordObj['password']=this.password;
      //  forgotPasswordObj['secure_token']=this.secure_token;
      //  var jsonString=JSON.stringify(forgotPasswordObj);
      //  wip(1);
       //login webservice
       var jsonString=JSON.stringify(this.forgetpwdObj);
       this.http.post<any>(this.data.WEBSERVICE+'/user/ResetPassword',jsonString,{headers: {
           'Content-Type': 'application/json'
         }})
      .subscribe(response=>{
        //  wip(0);
         var result=response;
         if(result.error.error_data!='0'){
           this.data.alert(result.error.error_msg,'danger');
         }else{
          /* $.confirm({
             title: 'Success',
             content: 'Password successfully reset',
             type: 'green',
             buttons: {
                 ok: function () {
                    window.location='login.html';
             }
           }
         });*/
  
         this.data.alert('Password successfully reset','success');
         this.route.navigateByUrl('/login');
  
         }
  
       },function(reason){
        //  wip(0);
         this.data.alert('Internal Server Error','danger')
       });
     }else{
      //$('.submit_btn').attr('disabled','disabled');
       this.data.alert('Please provide valid email','warning');
     }
   }

}
